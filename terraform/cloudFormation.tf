## Creacion de la VPC ##
resource "aws_cloudformation_stack" "eksVPC" {
  name = "eksVPC"

  template_url = "https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2018-11-07/amazon-eks-vpc-sample.yaml"
}

output "cf_output" {
    value = aws_cloudformation_stack.eksVPC.outputs
}

## Creacion de los worker nodes ##
resource "aws_cloudformation_stack" "eks-workernodes" {
  name = "eks-workernodes"

  template_body = data.template_file.eks_workers.rendered

  #capabilities = ["CAPABILITY_IAM"]
  capabilities = ["CAPABILITY_NAMED_IAM"]
}

data "template_file" "eks_workers" {
  template = "${file("${path.module}/eks_node_group.tpl")}"
  vars = {
    keyName = aws_key_pair.ekskeys.key_name
    eksClusterName = aws_eks_cluster.EKSDeepDive.name
    VPCId = aws_cloudformation_stack.eksVPC.outputs["VpcId"]
    CCPSecurityGroup = aws_cloudformation_stack.eksVPC.outputs["SecurityGroups"]
    subnetsIDs = aws_cloudformation_stack.eksVPC.outputs["SubnetIds"]
  }
}

output "nodeInstanceRole" {
  value = aws_cloudformation_stack.eks-workernodes.outputs["NodeInstanceRole"]
}

output "NodeSecurityGroup" {
  value = aws_cloudformation_stack.eks-workernodes.outputs["NodeSecurityGroup"]
}

resource "local_file" "aws-auth-cm" {
  filename        = "../kubernetes/aws-auth-cm.yaml"
  file_permission = "0644"
  content         = <<EOF
apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_cloudformation_stack.eks-workernodes.outputs["NodeInstanceRole"]}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes

  EOF
}

# Policies for Cluster Autoscaling
resource "aws_iam_policy" "policy-CA" {
  name        = "policy-CA"
  description = "A test policy for CA"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeInstances",
                "autoscaling:DescribeLaunchConfigurations",
                "autoscaling:DescribeTags",
                "autoscaling:SetDesiredCapacity",
                "autoscaling:TerminateInstanceInAutoScalingGroup",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:DescribeInstanceTypes"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "policy-attach-ca" {
  name       = "policy-attach-ca"
  roles      = ["NodeInstanceRole"]
  policy_arn = aws_iam_policy.policy-CA.arn
}
