resource "aws_eks_cluster" "EKSDeepDive" {
  name     = "EKSDeepDive"
  role_arn = aws_iam_role.eksServiceRole.arn

  vpc_config {
    subnet_ids = data.aws_subnet_ids.subnetsids.ids
    security_group_ids = ["${aws_cloudformation_stack.eksVPC.outputs["SecurityGroups"]}"]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.example-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.example-AmazonEKSServicePolicy,
    aws_cloudformation_stack.eksVPC,
  ]
}

output "endpoint" {
  value = aws_eks_cluster.EKSDeepDive.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.EKSDeepDive.certificate_authority.0.data
}

data "aws_subnet_ids" "subnetsids" {
  vpc_id = aws_cloudformation_stack.eksVPC.outputs["VpcId"]
}

## Inicio llaves ssh ##
resource "aws_key_pair" "ekskeys" {
  key_name   = "ekskeys"
  public_key = tls_private_key.ekskeys.public_key_openssh
}

resource "tls_private_key" "ekskeys" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ekskeys" {
  content         = tls_private_key.ekskeys.private_key_pem
  filename        = "temp.pem"
  file_permission = "0400"
}
## Fin llaves ssh ##
