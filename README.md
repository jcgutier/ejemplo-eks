# Ejemplo de Cluster de EKS

## Configuracion de la VPC:
Se recomienda crear una VPC separada para cada cluster, en este caso se creo usando cloudFormation con el template:
<https://amazon-eks.s3-us-west-2.amazonaws.com/cloudformation/2018-11-07/amazon-eks-vpc-sample.yaml>

![EKX_VPC](https://dl.dropboxusercontent.com/s/mrjctcfan5yhb6l/EKS_VPC.png)

## Pre-requistos
Se debe de tener lo siguiente antes de correr este ejemplo:

- Instalar kubectl
- Instalar awscli

## Cluster Deploy
Para correr este ejemplo hay que ejecutar los siguientes pasos:

1. Confgurar el acceso con las credenciales correctas: `aws configure`.
2. Aplicar los cambios de Terraform: `terraform plan/apply`.
3. Configurar el acceso de kubectl: `aws eks update-kubeconfig --name EKSDeepDive`
4. Agregar los workers al cluster: `kubectl apply -f aws-auth-cm.yaml`

## Kubernetes Dahsboard
Para iniciar el dashboard de kubernetes:

- kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.3/aio/deploy/recommended.yaml
- aws-iam-authenticator -i EKSDeepDive token
- kubectl apply -f eks-admin-service-account.yaml
- kubectl apply -f eks-admin-cluster-role-binding.yaml
- kubectl proxy

Lo caul nos deja acceder al dashboard por medio de: `http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/`
